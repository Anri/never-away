from pyautogui import moveTo, size as screen_size, press, position
from time import sleep
from threading import Thread
from math import radians, cos, sin


def action():
    while True:
        press("win")
        sleep(0.5)
        press("win")
        sleep(20)


def wait_mouse():
    previous_x, previous_y = position()
    threshold = 50

    while True:
        position_x, position_y = position()
        if (
            abs(position_x - previous_x) > threshold
            or abs(position_y - previous_y) > threshold
        ):
            return

        previous_x, previous_y = position_x, position_y
        sleep(0.1)


def circle(radius, base_x, base_y, wait):
    sleep(wait)
    while True:
        for angle in range(0, 360, 5):
            x = base_x + radius * cos(radians(angle))
            y = base_y + radius * sin(radians(angle))

            moveTo(x, y, 0.15)


if __name__ == "__main__":
    # Win key
    t1 = Thread(target=action)
    t1.daemon = True
    t1.start()

    wait = 5

    # Circle mouse
    t2 = Thread(
        target=circle, args=(100, screen_size()[0] // 2, screen_size()[1] // 2, wait)
    )
    t2.daemon = True
    t2.start()

    sleep(wait + 2)
    wait_mouse()
