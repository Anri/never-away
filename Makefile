HEADER = header.bat
PYTHON = main.py

OUTPUT = na.bat

all: build

build:
	cat $(HEADER) > $(OUTPUT)
	echo "" >> $(OUTPUT)
	echo "::python_beg" >> $(OUTPUT)
	cat $(PYTHON) >> $(OUTPUT)
	echo "" >> $(OUTPUT)
	echo "::python_end" >> $(OUTPUT)
