@ECHO OFF
SETLOCAL ENABLEDELAYEDEXPANSION

SET "tmpFile=%TEMP%\~na.py"

:: Variables
SET "pipR=pip install pyautogui"
SET "py=python.exe"

:: Check python installation
!py! --version >NUL

:: Clean potential error or python version
CLS

IF ERRORLEVEL 1 (
  ECHO Installation of Python not found, installation...

  :: Accept Winget ToT
  ECHO Y | winget list >NUL

  :: Install Python 3.13 from MS Store
  ECHO Y | winget install -he 9PNRBTZXMB4Z

  ECHO Download and installation of dependencies...

  :: Location of Python
  SET "py=%LOCALAPPDATA%\Microsoft\WindowsApps\!py!"

  :: Update pip
  !py! -m pip install --upgrade pip

  :: Install dependencies
  !py! -m !pipR!
) ELSE (
  :: Check dependencies
  !py! -m !pipR! >NUL
)

:: Write the program
CALL :getLine "::python_beg" "::python_end" > "!tmpFile!"

:: Run the program
PowerShell -Command "Start-Process cmd -Argument '/c START /B python !tmpFile!' -WindowStyle hidden"
EXIT /B

:getLine <beg str> <end str>
  SET "bBegEnd=0"
  FOR /F "usebackq delims=" %%i IN ("%~f0") do (
    IF !bBegEnd! EQU 1 (
      IF "%%i" EQU "%~2" ( EXIT /B )
      SETLOCAL DISABLEDELAYEDEXPANSION
      ECHO %%i
      ENDLOCAL
    ) ELSE (
      IF "%%i" EQU "%~1" ( SET "bBegEnd=1" )
    )
  )

ENDLOCAL
EXIT /B
